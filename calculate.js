"use strict";
//Licensed under the Apache License 2.0. See license file for information.
// Ardon Income Calculator v3.7 stable.
// Written by Josh Mudge
// Ad Mejorem Dei Glorium

function loadsaved() {

  var blocks = localStorage.blocks // Put this section in it's own function, pass the variables to the other functions when usevalues() is run.
  var soldiers = localStorage.soldiers
  var builders = localStorage.builders
  var one = localStorage.one
  var hotel = localStorage.hotel
  var gov = localStorage.gov
  var govspecial = localStorage.govspecial
  var indesign = localStorage.indesign
  var outdesign = localStorage.outdesign
  var embassies = localStorage.embassies
  var capital = localStorage.capital
  var infra = localStorage.infra
  var chain = localStorage.chain
  var town = localStorage.town
  var city = localStorage.city
  var metro = localStorage.metro

  document.getElementById("blocks").value = blocks;
  document.getElementById("soldiers").value = soldiers;
  document.getElementById("builders").value = builders;
  document.getElementById("one").value = one;
  document.getElementById("gov").value = hotel;
  document.getElementById("gov").value = gov;
  document.getElementById("indesign").value = indesign;
  document.getElementById("outdesign").value = outdesign;
  document.getElementById("embassies").value = embassies;
  document.getElementById("capital").value = capital;
  document.getElementById("infra").value = infra;
  document.getElementById("chain").value = chain;
  document.getElementById("town").value = town;
  document.getElementById("city").value = city;
  document.getElementById("metro").value = metro;

}

function usesaved() {
  var blocks = localStorage.blocks
  var soldiers = localStorage.soldiers
  var builders = localStorage.builders
  var one = localStorage.one
  var hotel = localStorage.hotel
  var gov = localStorage.gov
  var indesign = localStorage.indesign
  var outdesign = localStorage.outdesign
  var embassies = localStorage.embassies
  var capital = localStorage.capital
  var infra = localStorage.infra
  var chain = localStorage.chain
  var town = localStorage.town
  var city = localStorage.city
  var metro = localStorage.metro
  calculate();

}

function calculate() {
  var numReg = /^\d+$/;

  //Get input

  var blocks = document.getElementById("blocks").value;
  var soldiers = document.getElementById("soldiers").value;
  var builders = document.getElementById("builders").value;
  var one = document.getElementById("one").value;
  var hotel = document.getElementById("hotel").value;
  var gov = document.getElementById("gov").value;
  var indesign = document.getElementById("indesign").value;
  var outdesign = document.getElementById("outdesign").value;
  var embassies = document.getElementById("embassies").value;
  var capital = document.getElementById("capital").value;
  var infra = document.getElementById("infra").value;
  var chain = document.getElementById("chain").value;
  var town = document.getElementById("town").value;
  var city = document.getElementById("city").value;
  var metro = document.getElementById("metro").value;

  //Store values.

  localStorage.blocks = document.getElementById("blocks").value;
  localStorage.soldiers = document.getElementById("soldiers").value;
  localStorage.builders = document.getElementById("builders").value;
  localStorage.one = document.getElementById("one").value;
  localStorage.hotel = document.getElementById("hotel").value;
  localStorage.gov = document.getElementById("gov").value;
  localStorage.indesign = document.getElementById("indesign").value;
  localStorage.outdesign = document.getElementById("outdesign").value;
  localStorage.embassies = document.getElementById("embassies").value;
  localStorage.capital = document.getElementById("capital").value;
  localStorage.infra = document.getElementById("infra").value;
  localStorage.chain = document.getElementById("chain").value;
  localStorage.town = document.getElementById("town").value;
  localStorage.city = document.getElementById("city").value;
  localStorage.metro = document.getElementById("metro").value;

  if (blocks.match(numReg))
  {
    //Do nothing.
  }
  else
  {
    //Tells them to enter only numbers.

    alert("Please enter only numbers in this field.");
  }

  var gold = +blocks / 1000 * 100
  console.log(gold)
  var gold = +gold - (soldiers * 50) // Note to self: Fix possibiliy of giving bonuses to negative incomes.
  console.log(gold)
  var gold = +gold - (builders * 50) // Note to self: Fix possibiliy of giving bonuses to negative incomes.
  console.log(gold)
  var infra = +infra / 1000 * 0.0025
  console.log(infra)
  var one = +one / 1000 * 0.01
  console.log(one)
  var chain = +chain * 0.0025
  console.log(chain)
  var indesign = +indesign / 1000 * 0.02
  console.log(outdesign)
  var outdesign = +outdesign / 1000 * 0.015
  console.log(indesign)
  var hotel = +hotel * 0.015
  console.log(hotel)
  var gov = +gov * 0.015
  console.log(gov)
  var embassies = +embassies * 0.02
  console.log(embassies)
  var capital = +capital * 0.05 // Limit to 1 capital.
  console.log(capital)
  var town = +town * 0.005
  console.log(town)
  var city = +city * 0.01
  console.log(city)
  var metro = +metro * 0.01
  console.log(metro)
  var percent = +one + +indesign + +outdesign +hotel + +gov + +govspecial + +embassies + +capital + +infra + +chain + +town + +city + +metro
  console.log(percent)
  var bonus = +percent * 100
  var bonus = bonus.toFixed(2);
  console.log(bonus)
  var total = +gold * +percent
  console.log(total)
  var total = +total + +gold
  console.log(total)
  var total = total.toFixed(0);
  console.log(total)
  var emeralds = +total / 100
  console.log(emeralds)
  //var date = Date();
  //console.log(date)

  //Show result
  alert("Your Income is " + total + " Gold (or " + emeralds + " emeralds) per Minecraft Year (5 days)");
  document.getElementById("Bonus").innerHTML = "Your Efficiency bonus is " + bonus + "%";
  document.getElementById("Income").innerHTML = "Your Income is " + total + " Gold (or " + emeralds + " emeralds) per Minecraft Year (5 days)";

}
